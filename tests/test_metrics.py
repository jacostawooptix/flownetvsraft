from numpy.core.numeric import Inf, NaN
import pytest
import numpy as np
from numpy.random import RandomState
import torch
import os
import sys
THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(THIS_DIR_PATH))
from src.util.metrics import sens,espe,exac,calc_mse_mae,mae,mse


class TestMetrics:
    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass
    
    def test_dummy(self):
        assert 0 == 0
    

    #@pytest.mark.parametrize("m1", np.asarray([[0,1,0],[0,1,1], [1,1,0]]))
    #@pytest.mark.parametrize("m2", np.asarray([[0,1,0],[0,1,1], [1,1,0]]))
    def test_sens(self):
        m2 = np.asarray([[0,1,0],[0,1,1], [1,1,0]])
        m1 = np.asarray([[1,1,1],[1,1,1], [1,1,1]])
        a = sens(m1,m2)
        assert a == 5/9


    def test_espe(self):
        m2 = np.asarray([[0,1,0],[0,1,1], [1,1,0]])
        m1 = np.asarray([[0,0,0],[0,0,0], [0,0,0]])
        a = espe(m1,m2)
        assert a == 4/9


    
    def test_exa(self):
        m2 = np.asarray([[0,1,0],[0,1,1], [1,1,0]])
        m1 = np.asarray([[0,0,0],[0,0,0], [0,0,0]])
        a = exac(m1,m2)
        assert a == (4 + 0)/(9 + 0)


    def test_mae(self):
        m2 = np.asarray([[0,1,0],[0,1,1], [1,0,1]])
        m1 = np.asarray([[0,0,0],[0,0,0], [0,NaN,0]])
        a = mae(m1,m2)
        assert a == 5/8

    def test_mse(self):
        m2 = np.asarray([[0,1,0],[0,1,1], [1,0,1]])
        m1 = np.asarray([[0,NaN,0],[0,Inf,0], [0,NaN,0]])
        a = mse(m1,m2)
        assert a == 3/6

    def test_calc(self):
        mse,mae = calc_mse_mae(THIS_DIR_PATH)
        assert mse == 0.938394804913721
        assert mae == 0.9931714748226823



