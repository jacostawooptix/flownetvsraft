import pytest
import numpy as np
import torch
import os
import sys
import cv2
import glob
THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(THIS_DIR_PATH))
from src.data.preproces_func import crop_img,resizeimg,resize_disp,tojson


class TestImg_functions:
    @classmethod
    def setup_class(cls):
        a = np.ones((1024,1024))
        cv2.imwrite('test.png',a)
        pass

    @classmethod
    def teardown_class(cls):
        os.chdir(os.path.normpath(os.path.join(THIS_DIR_PATH,'..')))
        os.remove('test.png')
        os.chdir(THIS_DIR_PATH)
        os.remove('test.json')
        pass
    
    def test_crop_img(self):
        a = cv2.imread('test.png',-1)
        f = crop_img(a,20)
        assert f.shape[0] == (a.shape[0] // 20) * 20
        assert f.shape[1] == (a.shape[0] // 20) * 20

    
    def test_resize_img(self):
        a = cv2.imread('test.png',-1)
        f = resizeimg(a,320)
        assert f.shape[0] == 320
        assert f.shape[1] == 320
        

   
    def test_resize_disp(self):
        a = cv2.imread('test.png',-1)
        f = resize_disp(a,1280)
        assert f.shape[0] == 1280
        assert f.shape[1] == 1280
        assert f[0][0] > 1

    
    def test_tojson(self):
        tojson(THIS_DIR_PATH,'a','a','a','test','test')
        assert len(glob.glob('*.json')) > 0