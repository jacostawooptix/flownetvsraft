from numpy.core.numeric import Inf, NaN
import pytest
import numpy as np
from numpy.random import RandomState
import torch
import os
import sys
THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(THIS_DIR_PATH))
from src.util.flownet_argparser import arg_parser


class TestArgsparser:
    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass
    
    def test_flow_argparser(self):
        x = arg_parser('2','3','4','/home/jacosta/Desktop/flownetvsRAFT/scripts',flag=False)
        assert x.out == '2'
        assert x.im1 == '4'
        assert x.im2 == '3'
        x = arg_parser('2','3','4','/home/jacosta/Desktop/flownetvsRAFT/scripts',flag=True)
        assert x.out == '2'
        assert x.im1 == '3'
        assert x.im2 == '4'
        
    
