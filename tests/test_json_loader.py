from numpy.core.numeric import Inf, NaN
import pytest
import numpy as np
from numpy.random import RandomState
import torch
import os
import sys
import json
THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(THIS_DIR_PATH))
from src.flow.json_loader import json_loader


class TestJson_loader:
    @classmethod
    def setup_class(cls):
        x = { 
		'img1path':'1',
		'img2path':'2',
		'img3path':'3',
		'folderpath':'4'
        }
        aux =  'test.json'
        json_file = open(aux, "w")
        json.dump(x, json_file, indent=4, sort_keys=True)
        json_file.close()
        pass

    @classmethod
    def teardown_class(cls):
        os.remove('test.json')
        pass
    
    def test_json(self):
        a,b,c,d = json_loader('test.json')
        assert a == '1'
        assert b == '2'
        assert c == '3'
        assert d == '4'
       
        
    
