#!/usr/bin/env python3
import os
import glob


def download_dataset(datapath):
	# Read links from txt file
	links_file_name = "dataset_links.txt"
	aux = os.path.normpath(
		os.path.join(datapath,"..","data","raw")
	)

	os.chdir(aux)
	# Download all selected links to local drive
	with open(links_file_name, 'r') as links_file:
		
		link = links_file.readline()
		
		while link:
			
			# download file 
			ret = os.system('wget -c ' + link)
			
			if ret == 0:
				link = links_file.readline()

	# unzipping all downloaded file
	# Getting all filenames
	zip_names = glob.glob('*.zip')

	for filename in zip_names:
		os.system('unzip ' + filename) # unzipping
		os.system('rm ' + filename) # removing zip file to save space
		
	os.chdir(datapath)
