#!/usr/bin/env python3
import os
import glob
import cv2
import re
import sys
import json
import numpy as np
from shutil import copy
from shutil import rmtree
from shutil import copyfile
from PIL import Image


def crop_img(img1, align):
    h = img1.shape[0]
    w = img1.shape[1]
    w = (w // align) * align
    h = (h // align) * align

    return img1[:h,:w]
# devuelve una copia


def resizeimg(
	img1, max_size=1280):
	h = img1.shape[0]
	w = img1.shape[1]
	k = w // h
	w = max_size
	h = w // k

	dim = (w,h)
	img1 = cv2.resize(img1,dim)
	return img1


def resize_disp(img1,max_size=1280):
	w_disp = img1.shape[1]
	img1 = resizeimg(img1, max_size)
	w_newdisp = img1.shape[1]
	factor = w_newdisp / w_disp
	img1 = img1 * factor
	return img1



def tojson(
		datapath, path1, path2, path3,folderpath, name):
	os.chdir(datapath)
	x = { 
		'img1path':path1,
		'img2path':path2,
		'img3path':path3,
		'folderpath':folderpath
	}
	aux = name + '.json'
	json_file = open(aux, "w")
	json.dump(x, json_file, indent=4, sort_keys=True)
	json_file.close()
  

