#!/usr/bin/env python3
import glob
import json
import numpy as np
import os
import cv2
from thirdparty.consistent_depth.utils.consistency import consistent_flow_masks

def consistency(s,s1,THIS_DIR_PATH,datapath_result1,datapath_result_flow,im1,im2,flag=False):
    disparity1 = cv2.imread(datapath_result1,-1)
    disparity2 = cv2.imread(datapath_result_flow,-1)
    if flag:
        disparity1 = np.stack([disparity1, np.zeros_like(disparity1)], -1)
        disparity2 = np.stack([disparity2, np.zeros_like(disparity2)], -1)
        consistent_mask = consistent_flow_masks([disparity1,disparity2],[np.float32(im1)/255,np.float32(im2)/255],1,1)
    else:
        consistent_mask = consistent_flow_masks([disparity1[...,0:2],disparity2[...,0:2]],[np.float32(im1)/255,np.float32(im2)/255],1,1)
    aux = np.asarray(consistent_mask)
    aux = np.moveaxis(aux,0,-1)
    masks_out = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "consistency_masks",s))
    cv2.imwrite(masks_out,aux[...,0] * 255)
    masks_out = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "consistency_masks",s1))
    cv2.imwrite(masks_out,aux[...,1] * 255)