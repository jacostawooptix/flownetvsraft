#!/usr/bin/env python3
import glob
import json
import numpy as np 


def json_loader(
    name):
    with open(name) as file:
        data = json.load(file)
        return data["img1path"],data["img2path"],data["img3path"],data["folderpath"]