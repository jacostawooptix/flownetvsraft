#!/usr/bin/env python3
import argparse
import os
import sys

def args_parser(out1,out2,path_to_data):
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', help="restore checkpoint", default="model/raft-sintel.pth")
    parser.add_argument('--path', help="dataset for evaluation", default=path_to_data)
    parser.add_argument('--small', action='store_true', help='use small model')
    parser.add_argument('--mixed_precision', action='store_true', help='use mixed precision')
    parser.add_argument('--alternate_corr', action='store_true', help='use efficent correlation implementation')
    parser.add_argument('--out1', action='store_true', help='output1 path', default=out1)
    parser.add_argument('--out2', action='store_true', help='output2 path', default=out2)
    args_raft = parser.parse_args(sys.argv[1:])
    return args_raft