#!/usr/bin/env python3
from math import isnan
import os
import sys
import glob
import cv2
import numpy as np
import cv2
from numpy.core.numeric import zeros_like
from sklearn.metrics import mean_squared_error
THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(THIS_DIR_PATH))


def calc_mse_mae(THIS_DIR_PATH):
    datapath_raft = os.path.normpath(os.path.join(THIS_DIR_PATH,"..", "data", "result","RAFT"))
    datapath_flow = os.path.normpath(os.path.join(THIS_DIR_PATH,"..", "data", "result","flownet"))
    os.chdir(datapath_flow)
    filenames = glob.glob('*-*1.exr')
    mse_flow_acum = 0
    mse_raft_acum = 0
    mae_flow_acum = 0
    mae_raft_acum = 0
    for filename in filenames:
        s = filename.split('1.')[0]
        s1 = s + '.exr'
        img_Rflow = cv2.imread(filename,-1)
        img_Lflow = cv2.imread(s1,-1)
        os.chdir(datapath_raft)
        img_Rraft = cv2.imread(filename,-1)
        img_Lraft = cv2.imread(s1,-1)

        datapath_img = os.path.normpath(os.path.join(THIS_DIR_PATH, "..","data", "interm", s))
        os.chdir(datapath_img)
        perf = cv2.imread('disp.exr',-1)
        mse1_flow = mse(perf,img_Rflow)
        mse2_flow = mse(perf,img_Lflow)
        mse2_raft = mse(perf,img_Lraft[...,0])
        mse1_raft = mse(perf,img_Rraft[...,0])
        mae1_flow = mae(perf,img_Rflow)
        mae2_flow = mae(perf,img_Lflow)
        mae2_raft = mae(perf,img_Lraft[...,0])
        mae1_raft = mae(perf,img_Rraft[...,0])
        #print(s + 'mse(f,R,f,R) ')
        #print(mse1_flow,mse1_raft,mse2_flow,mse2_raft)
        #print(mae1_flow,mae1_raft,mae2_flow,mae2_raft)

        mse_raft_acum = mse_raft_acum + mse1_raft + mse2_raft
        mse_flow_acum = mse_flow_acum + mse1_flow + mse2_flow
        mae_raft_acum = mae_raft_acum + mae1_raft + mae2_raft
        mae_flow_acum = mae_flow_acum + mae1_flow + mae2_flow
        
        os.chdir(datapath_flow)
    print('flownet/raft')
    print(mse_flow_acum/mse_raft_acum)
    print(mae_flow_acum/mae_raft_acum)
    return mse_flow_acum/mse_raft_acum,mae_flow_acum/mae_raft_acum


def mse(perf,result):
    where_is_valid = (~np.isnan(perf) & ~np.isinf(perf))
    mse = np.square((perf[where_is_valid] - result[where_is_valid])**2).mean()   
    return mse


def mae(perf,result):
    where_is_valid = (~np.isnan(perf) & ~np.isinf(perf))
    mae  = np.absolute(perf[where_is_valid] - result[where_is_valid]).mean()
    return mae

def sens(im1,im2):
    VP = (np.logical_and(im1,im2).sum())
    P = im1.sum()
    if P != 0:
        return (VP / P)


def espe(im1,im2):
    VN = (np.logical_not(np.logical_or(im1,im2)).sum())
    H,W = im1.shape
    N = (H * W)- np.sum(im1)
    if N != 0:
        return (VN / N)


def exac(im1,im2):
    VN = (np.logical_not(np.logical_or(im1,im2)).sum())
    H,W = im1.shape
    N = (H * W)- np.sum(im1)   
    VP = (np.logical_and(im1,im2).sum())
    P = im1.sum()
    if N + P != 0:
        return ((VN + VP)/(N + P))

