#!/usr/bin/env python3
import glob
import json
import cv2
import os
import sys
import argparse
import torch
import numpy as np 


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

def arg_parser(out,path_im1,path_im2,THIS_DIR_PATH,flag=False):
    args = dotdict()
    checkpoint = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "thirdparty", "consistent_depth", "pretrained_models","FlowNet2_checkpoint.pth.tar" ))
    args.pretrained_model_flownet2  = checkpoint
    if flag:
        args.out = out
        args.im1 = path_im1
        args.im2 = path_im2
    else:
        args.out = out
        args.im1 = path_im2
        args.im2 = path_im1
    args.fp16 = False
    args.homography = 'KITTI' not in checkpoint
    args.rgb_max = 255.0
    args.visualize = False
    return args
    