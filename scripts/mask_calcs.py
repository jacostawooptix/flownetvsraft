#!/usr/bin/env python3
import glob
import json
import cv2
import os
import sys
import numpy as np
THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(THIS_DIR_PATH))
from src.util.metrics import sens,espe,exac,calc_mse_mae



def calc_consistency(s,perf,consistency_mask):
    im = cv2.imread(consistency_mask,-1)
    im = im / 255
    sen = sens(perf,im)
    esp = espe(perf,im)
    exa = exac(perf,im)
    print(s,sen,esp,exa)
    return sen,esp,exa


def statistics():
    check = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "consistency_masks"))
    os.chdir(check)
    check = glob.glob('*')
    if not check:
        print('There are not any consitency mask calculated yet')
    else:
        datapath_consistency = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "consistency_masks"))
        os.chdir(datapath_consistency)
        filenames = glob.glob('*_raft.png')
        esp_acum_raft = 0
        sen_acum_raft = 0
        exa_acum_raft = 0
        esp_acum_flow = 0
        sen_acum_flow = 0
        exa_acum_flow = 0
        counter = 0
        for filename in filenames:
            counter = counter + 1
            s = filename.split('_')[0]
            datapath_masks = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "consistency_masks", filename))
            datapath_perf = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "interm", s, "disp.exr"))
            perf = cv2.imread(datapath_perf,-1)
            where_are_NaNs = np.isnan(perf)
            perf[where_are_NaNs] = 0
            where_are_inf = np.isinf(perf)
            perf[where_are_inf] = 0
            perf[~where_are_NaNs & ~where_are_inf] = 1
            calc_consistency(filename,perf,datapath_masks)
            s = s + "_raft1.png"
            datapath_masks = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "consistency_masks", s)) 
            calc_consistency(s,perf,datapath_masks)
            s = filename.split('_')[0]
            s = s + '_flow.png'
            datapath_masks = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "consistency_masks", s))
            calc_consistency(s,perf,datapath_masks)
            s = filename.split('_')[0]
            s = s + '_flow1.png'
            datapath_masks = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "consistency_masks", s))
            calc_consistency(filename,perf,datapath_masks)
        calc_mse_mae(THIS_DIR_PATH)

statistics()