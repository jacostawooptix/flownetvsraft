#!/usr/bin/env python3
import os
import sys
import glob
import cv2
import numpy as np
import re
PATH_TO_THIS_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(PATH_TO_THIS_DIR))

from src.data.preproces_func import crop_img
from src.data.dataset_maker import download_dataset
from src.data.preproces_func import tojson
from src.data.preproces_func import resizeimg
from src.data.preproces_func import resize_disp

def intermediate(
		imgD, imgL, imgR,
		folder_name, datapath):

    aux = os.path.normpath(
			os.path.join(datapath, ".." ,"data" ,"interm")
	)
    os.chdir(aux)
    try:
    	os.mkdir(folder_name)
    	print(folder_name + "Created STATUS:OK\n")
    except OSError as error:
    	print("Fail creating " + folder_name + " STATUS:ERROR(File already create)\n")   
    aux = os.path.normpath(
			os.path.join(aux, folder_name)
	)
    os.chdir(aux)
    cv2.imwrite("im0.png", imgD)
    cv2.imwrite("im1.png", imgL)
    cv2.imwrite("disp.exr", imgR)
    os.chdir(datapath)
    

def read_pfm(file):
	file = open(file, 'rb')
  
	color = None
	width = None
	height = None
	scale = None
	endian = None

	header = file.readline().rstrip()
	if header.decode('ascii') == 'PF':
		color = True    
	elif header.decode('ascii') == 'Pf':
		color = False
	else:
		raise Exception('Not a PFM file.')

	dim_match = re.search(r'(\d+)\s(\d+)', file.readline().decode('ascii'))
	if dim_match:
		width, height = map(int, dim_match.groups())
	else:
		raise Exception('Malformed PFM header.')

	scale = float(file.readline().rstrip())
	if scale < 0: # little-endian
		endian = '<'
		scale = -scale
	else:
		endian = '>' # big-endian

	data = np.fromfile(file, endian + 'f')
	shape = (height, width, 3) if color else (height, width)
	return np.reshape(data, shape), scale

def init():
    THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
    #download_dataset(THIS_DIR_PATH) # Uncomment this if u want to download the data
    datapathraw = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "raw"))
    os.chdir(datapathraw)
    filenames = glob.glob('*-*')
    for filename in filenames:
        datapath = os.path.normpath(os.path.join(datapathraw, filename))
        os.chdir(datapath)
        datapathR = os.path.normpath(os.path.join(datapath, "im1.png"))
        img_D = resizeimg(cv2.imread(datapathR),1024)
        img_D = crop_img(img_D, 64)
        datapathL = os.path.normpath(os.path.join(datapath, "im0.png"))
        img_L = resizeimg(cv2.imread(datapathL),1024)
        img_L = crop_img(img_L, 64)
        img_R = read_pfm("disp0.pfm")
        img_R = resize_disp(img_R[0][::-1],1024)
        img_R = crop_img(img_R, 64)
        intermediate(img_D, img_L, img_R, filename, THIS_DIR_PATH)
        datapathR = os.path.normpath(
            os.path.join(THIS_DIR_PATH, "..", "data", "interm", filename, "im1.png")
        )
        datapathL = os.path.normpath(
            os.path.join(THIS_DIR_PATH, "..", "data", "interm", filename, "im0.png")
        )
        datapathF = os.path.normpath(
            os.path.join(THIS_DIR_PATH, "..", "data", "interm", filename,"disp.exr")
        )
        folderpath = os.path.normpath(
            os.path.join(THIS_DIR_PATH, "..", "data", "interm", filename)
        )
        datapath_processed = os.path.normpath(os.path.join(datapathraw, "..", "processed"))
        tojson(datapath_processed, datapathR, datapathL, datapathF,folderpath, filename)
        os.chdir(THIS_DIR_PATH)

if __name__ == "__main__":
    init()
else:
    init()
