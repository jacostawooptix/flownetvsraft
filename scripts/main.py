#!/usr/bin/env python3
import os 
import sys
THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(THIS_DIR_PATH))
from init import init 
from flownetvsraft import flownet_vs_raft 
from mask_calcs import statistics



def main():
    init()
    print('Loading results...')
    flownet_vs_raft()
    print('Calculating the MSE and MAE and spe,sen,exa')
    statistics()

if __name__ == "__main__":
    main()
