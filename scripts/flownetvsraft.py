#!/usr/bin/env python3
import glob
import json
import cv2
import os
import sys
import torch
import numpy as np 
from PIL import Image
PATH_TO_THIS_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(PATH_TO_THIS_DIR))
from thirdparty.RAFT.demo import demo
from thirdparty.consistent_depth.optical_flow_flownet2_homography import process
from thirdparty.consistent_depth.utils.consistency import consistent_flow_masks
from src.flow.json_loader import json_loader
from src.flow.consistency import consistency
from src.util.flownet_argparser import arg_parser
from src.util.raft_argparser import args_parser


def flownet_vs_raft():
    THIS_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
    check = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "processed"))
    os.chdir(check)
    check = glob.glob('*')
    if not check:
        print('There are not any data to process')
    else:
        datapath_processed = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "processed"))
        os.chdir(datapath_processed)
        filenames = glob.glob('*-*')
        for filename in filenames:
            images = json_loader(filename)
            
            #Flownet
            s = filename.split('.')[0]
            print('Processing result of ' + s + '...')
            s = s + '.exr'
            datapath_result_flow = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "result", "flownet",s ))
            process(arg_parser(datapath_result_flow,images[0],images[1],THIS_DIR_PATH))

            s = filename.split('.')[0]
            s = s + "1.exr"
            datapath_result_flow1 = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "result", "flownet",s ))
            process(arg_parser(datapath_result_flow1,images[0],images[1],THIS_DIR_PATH,True))

            #RAFT
            path = images[3]
            s = filename.split('.')[0]
            s = s + '.exr'
            out1 = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "result", "RAFT",s))
            s1 = filename.split('.')[0]
            s1 = s1 + '1.exr'
            out2 = os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "data", "result", "RAFT",s1))
            os.chdir(os.path.normpath(os.path.join(THIS_DIR_PATH, "..", "thirdparty",'RAFT')))
            demo(args_parser(out1,out2,images[3]))
            os.chdir(datapath_processed)

            #Consistency mask Flownet
            s = filename.split('.')[0]
            s = s + '_flow.png'
            s1 = filename.split('.')[0]
            s1 = s1 + '_flow1.png'
            im2 = cv2.imread(images[1],-1)
            im1 = cv2.imread(images[0],-1)
            consistency(s,s1,THIS_DIR_PATH,datapath_result_flow1,datapath_result_flow,im1,im2,True)

            #Consistency mask RAFT
            s = filename.split('.')[0]
            s = s + '_raft.png'
            s1 = filename.split('.')[0]
            s1 = s1 + '_raft1.png'
            consistency(s,s1,THIS_DIR_PATH,out1,out2,im1,im2)
        

if __name__ == "__main__":
    flownet_vs_raft()
else:
    flownet_vs_raft()
